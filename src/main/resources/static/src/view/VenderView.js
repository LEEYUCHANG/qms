import View from './View.js'
import Formatter from '../util/Formatter.js'

const TAG = '[VenderView]'

const VenderView = Object.create(View);

VenderView.setup = function (el) {
  this.init(el);
  this.binding();
  return this;
}

VenderView.render = function (data) {
  log(data)
  const userId = localStorage.getItem('userId');
  this.el.innerHTML = `
  <br>
  <div class="container">
    <div class="card">
      <div class="card-header"><h2>모바일 순번 대기표</h2></div>
      <div class="card-body">
        <h3>진료창구 : ${data.name}</h3><br>
        <h3>대기번호 : ${VenderView.myNumber(data.waitingUsers)}번</h3><br> 
        <h3>대기고객 : ${data.waitingUsers.length}명</h3><br>
        <h5>발행시간 : ${Formatter.formatDate(data.waitingUsers.pop().createdAt)}</h5><br>
        <h5>영업점 내점 시 번호표가 이미 호출된 경우 번호표를 재발행 하셔야 합니다.</h5>
        <h5>${userId}</h5>
      </div>
      <div class="card-footer">
        <button class="btn btn-primary btn-block" data-sectionid="${data.name}" data-userid="${userId}">대기 취소</button>
      </div>
    </div>
  </div>
  `; 
}

VenderView.myNumber = function (users) {
  const userId = localStorage.getItem('userId');
  return users.filter(u => u.id === userId).map(u => u.myNumber);
}

VenderView.binding = function () {
  this.on('click', function (e) {
    e.preventDefault()
    const data = { venderId: 'WOORI', subVenderId: 'LA', sectionId: e.target.dataset.sectionid, userId:e.target.dataset.userid }
    log(data);
    VenderView.emit('@cancel', data)
   });
}

export default VenderView;