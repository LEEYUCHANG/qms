export default {
  formatDate(createdAt) {
    let today = new Date(createdAt);
    let year = today.getFullYear();
    let month = today.getMonth() + 1;
    if (month < 10) month = '0' + month;
    let date = today.getDate();
    if (date < 10) date = '0' + date;
    let hour = today.getHours();
    if (hour < 10) hour = '0' + hour;
    let min = today.getMinutes();
    if (min < 10) min = '0' + min;
    let sec = today.getSeconds();
    if (sec < 10) sec = '0' + sec;
    return year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec;
  }
}