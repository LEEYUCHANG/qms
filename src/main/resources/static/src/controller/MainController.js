import MsgView from '../view/MsgView.js'
import VenderView from '../view/VenderView.js'
import VenderModel from '../model/VenderModel.js'
import EmitterModel from '../model/EmitterModel.js'
import VibrateModel from '../model/VibrateModel.js'
import MobileModel from '../model/MobileModel.js'

import { BASE_URL } from '../config.js'

export default {
  init() {

    // 화면 초기화
    VenderView
      .setup(document.getElementById('venderView'))
      .on('@cancel', data => this.onCancelWaiting(data.detail))
    
    // 메세지 표시화면 설정
    MsgView.setup(document.getElementById('msgView'))

    // 화면표시 데이터 습득
    this.getData()

  },

  getData() {
    const data = new FormData(document.getElementById('requestParms'))
    let dataObj = {}
    for (const [k, v] of data.entries()) {
      Object.assign(dataObj, { [k]: v })
    }

    VenderModel.findOne(BASE_URL + 'webapp', dataObj)
      .then((data) => {
        localStorage.setItem('userId', data.waitingUsers[data.waitingUsers.length - 1].id)
        return data
      })
      .then((data) => VenderView.render(data))
      .then(() => {
        // Server Side Event를 전달 받기 위해 서버에 연결
        EmitterModel
          .setup(new EventSource(BASE_URL + '/emitter' + '?userId=' + localStorage.getItem('userId')))
          .on('message', e => this.onMessage(e))
          .on('open', e => log('connection opened'))
          .on('error', e => {
            e.readyState == EventSource.CLOSED ? log('connection closed') : log('Error occured', e), e.target.close()
          })
      })
      .catch((error) => log(error))
  },

  onVibrate() {
    VibrateModel.startVibrate(1000)
  },

  onCancelWaiting(data) {
    this.onVibrate()
    let { venderId, subVenderId, sectionId } = data;
    const dataObj = {venderId, subVenderId, sectionId}

    VenderModel.cancelWaiting(BASE_URL + 'webapp' + '/' + data.userId, dataObj)
      .then(() => MsgView.render({ message: '', details: '대기가 취소 되었습니다.' }))
      .then(() => VenderView.hide())
      .catch(e => log(e))
  },

  onMessage(e) {
    // this.onVibrate()
    const msg = e.data
    log(msg)
    if (msg.includes('END_OPERATION')) {
      MsgView.render({ message: '', details: '진료가 완료 되었습니다.' })
      VenderView.hide()
    } else {
      const { venderId, subVenderId, sectionId } = JSON.parse(msg)
      // const { venderId, subVenderId, sectionId } = msg
      MobileModel
        .findOne(`${BASE_URL}webapp/${venderId}/${subVenderId}/${sectionId}/section`)
        .then(data => VenderView.render(data))
        .catch(error => log(error));
    }
  },
}
