export default {
    // Starts vibration at passed in level
    startVibrate(duration) {
      navigator.vibrate(duration)
      window.navigator.vibrate(duration) && window.navigator.vibrate(duration);
    },
  
    // Stops vibration
    stopVibrate() {
      // Clear interval and stop persistent vibrating
      if (vibrateInterval) clearInterval(vibrateInterval)
      navigator.vibrate(0)
    },
  
    // Start persistent vibration at given duration and interval
    // Assumes a number value is given
    startPersistentVibrate(duration, interval) {
      vibrateInterval = setInterval(function () {
        startVibrate(duration)
      }, interval)
    },
}