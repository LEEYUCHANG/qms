export default {
  async findOne(url, data) {
    const res = await fetch(url, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }  
    });
    if (!res.ok) throw new Error(await res.text());
    return await res.json();
  },
  async cancelWaiting(url, data) {
    const res = await fetch(url, {
      method: 'delete',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    })
    if (!res.ok) throw new Error(await res.text());
    // return await res.json();
  },

}