export default {
  async findOne(url, data) {
    const res = await fetch(url, {
      method: 'get',
    })
    if (!res.ok) throw new Error(await res.text());
    return await res.json();
  },
}