export default {
  setup(el) {
    if (!el) throw el;
    this.el = el;
    return this;
  },
  on(event, handler) {
    this.el.addEventListener(event, handler);
    return this;
  },
  
}