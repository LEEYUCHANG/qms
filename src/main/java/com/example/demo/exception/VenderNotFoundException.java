package com.example.demo.exception;

public class VenderNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public VenderNotFoundException() {
  }

  public VenderNotFoundException(String message) {
    super(message);
  }

  public VenderNotFoundException(Throwable cause) {
    super(cause);
  }

  public VenderNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public VenderNotFoundException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}

