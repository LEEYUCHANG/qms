package com.example.demo.exception;

public class SectionNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public SectionNotFoundException() {
  }

  public SectionNotFoundException(String message) {
    super(message);
  }

  public SectionNotFoundException(Throwable cause) {
    super(cause);
  }

  public SectionNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public SectionNotFoundException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
