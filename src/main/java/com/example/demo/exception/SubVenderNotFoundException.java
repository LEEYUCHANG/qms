package com.example.demo.exception;

public class SubVenderNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public SubVenderNotFoundException() {
  }

  public SubVenderNotFoundException(String message) {
    super(message);
  }

  public SubVenderNotFoundException(Throwable cause) {
    super(cause);
  }

  public SubVenderNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public SubVenderNotFoundException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
