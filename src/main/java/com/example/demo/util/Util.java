package com.example.demo.util;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.java.Log;

@Log
public class Util {
  public static boolean hashByMd5(String hash)
      throws NoSuchAlgorithmException, UnsupportedEncodingException {
    String input = LocalDate.now().toString();
    log.info(input);

    // MD5 MessageDigest의 생성
    MessageDigest mdMD5 = MessageDigest.getInstance("MD5");

    // " Java 마스터! " 문자열 바이트로 메시지 다이제스트를 갱신
    mdMD5.update(input.getBytes("UTF-8"));

    // 해시 계산 반환값은 바이트 배열
    byte[] md5Hash = mdMD5.digest();

    // 바이트배열을 16진수 문자열로 변환하여 표시
    StringBuilder hexMD5hash = new StringBuilder();

    for (byte b : md5Hash) {
      String hexString = String.format("%02x", b);
      hexMD5hash.append(hexString);
    }
    return hexMD5hash.toString().equals(hash);
  }

  public static byte[] getQRCodeImage(String text, int width, int height) {
		try {
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
			return byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

}
