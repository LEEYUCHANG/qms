package com.example.demo.listener;

import com.example.demo.event.SectionChangedEvent;
import com.example.demo.service.SseEmitterService;
import com.example.demo.vo.EmitterNotifyAllDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class SectionChangedEventListener implements ApplicationListener<SectionChangedEvent> {

  private final ObjectMapper mapper;
  private final SseEmitterService emitterService;

  @Override
  public void onApplicationEvent(SectionChangedEvent event) {

    if (event.getMessage().equals("END_OPERATION")) {
      emitterService.notify(event.getUserId(), event.getMessage());
      emitterService.remove(event.getUserId());
      return;
    }

    EmitterNotifyAllDto obj = null;
    try {
      obj = mapper.readValue(event.getMessage(), EmitterNotifyAllDto.class);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    emitterService.notifyAll(
        obj.getVenderId(),
        obj.getSubVenderId(), 
        obj.getFromSectionId(), 
        obj.getToSectionId());
  }
}
