package com.example.demo.service;

import static java.util.stream.Collectors.toList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.example.demo.entity.Section;
import com.example.demo.entity.SubVender;
import com.example.demo.entity.Vender;
import com.example.demo.repository.VenderRepository;
import com.example.demo.vo.FormDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

@Log
@Service
@RequiredArgsConstructor
public class SseEmitterService {

  private final ObjectMapper mapper;
  private final VenderRepository venderRepo;

  private Map<String, SseEmitter> eventMap = new HashMap<>();

  public void add(String userId, SseEmitter emitter) {
    eventMap.put(userId, emitter);
  }

  public void showMap() {
    eventMap.forEach((k, v) -> log.warning(String.format("%s : %s", k, v)));
  }

  public void notify(String userId, String msg) {
    eventMap.forEach((k, v) -> {
      if (k.equals(userId)) {
        // create a single thread for sending messages asynchronously
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
          try {
            v.send(SseEmitter.event().id("").name("message").data(msg).reconnectTime(10000));
          } catch (Exception e) {
            v.completeWithError(e);
            v.complete();
          }
        });
        executor.shutdown();
      }
    });
  }

  public void notifyAll(String venderId, String subVenderId, String fromSectionId,
      String toSectionId) {

    var deadEmitters = new ConcurrentHashMap<String, SseEmitter>();

    var vender = findVender(venderId);
    var subVender = findSubVender(subVenderId, vender);
    var fromSection = findSection(fromSectionId, subVender);
    var toSection = findSection(toSectionId, subVender);

    List<String> fromUserIds =
        fromSection.getWaitingUsers().stream().map(u -> u.getId()).collect(toList());

    List<String> toUserIds =
        toSection.getWaitingUsers().stream().map(u -> u.getId()).collect(toList());

    ExecutorService executor = Executors.newCachedThreadPool();

    eventMap.forEach((k, v) -> {

      // 스레드 1 송신
      executor.execute(() -> {
        if (fromUserIds.contains(k)) {
          try {
            v.send(SseEmitter.event().id("").name("message").data(mapper.writeValueAsString(new FormDto(venderId, subVenderId, fromSectionId))).reconnectTime(10000));
          } catch (Exception e) {
            deadEmitters.put(k, v);
          }
        }
      });

      // 스레드 2 송신
      executor.execute(() -> {
        if (toUserIds.contains(k)) {
          try {
            v.send(SseEmitter.event().id("").name("message").data(mapper.writeValueAsString(new FormDto(venderId, subVenderId, toSectionId))).reconnectTime(10000));
          } catch (Exception e) {
            deadEmitters.put(k, v);
          }
        }
      });

    });

    deadEmitters.forEach((k, v) -> {
      eventMap.remove(k);
    });

    executor.shutdown();
  }

  public void reset() {
    eventMap.forEach((k, v) -> {
      // create a single thread for sending messages asynchronously
      ExecutorService executor = Executors.newSingleThreadExecutor();
      executor.execute(() -> v.complete());
      executor.shutdown();
    });
    eventMap.clear();
  }

  public void remove(String userId) {
    eventMap.forEach((k, v) -> {
      if (k.equals(userId)) {
        // create a single thread for sending messages asynchronously
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
          v.complete();
        });
        executor.shutdown();
      }
    });
  }

  public void remove(SseEmitter sseEmitter) {
    eventMap.forEach((k, v) -> {
      if (v.equals(sseEmitter)) {
        eventMap.remove(k);
      }
    });
  }

  private Vender findVender(String venderId) {
    return venderRepo.findByName(venderId.toUpperCase());
  }

  private SubVender findSubVender(String subVenderId, Vender vender) {
    return vender.getSubVenders().stream()
        .filter(v -> v.getName().equals(subVenderId.toUpperCase())).findFirst().orElseThrow();
  }

  private Section findSection(String sectionId, SubVender subVender) {
    return subVender.getSections().stream().filter(s -> s.getName().equals(sectionId.toUpperCase()))
        .findFirst().orElseThrow();
  }

}
