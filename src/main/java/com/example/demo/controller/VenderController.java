package com.example.demo.controller;

import java.time.format.DateTimeFormatter;
import java.util.Objects;
import com.example.demo.entity.Section;
import com.example.demo.entity.SubVender;
import com.example.demo.entity.Vender;
import com.example.demo.event.SectionChangedEvent;
import com.example.demo.repository.VenderRepository;
import com.example.demo.vo.EmitterNotifyAllDto;
import com.example.demo.vo.FormDto;
import com.example.demo.vo.MyUser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

@Log
@Controller
@CrossOrigin("*")
@RequiredArgsConstructor
public class VenderController {

  private final ObjectMapper mapper;
  private final VenderRepository venderRepo;
  private final ApplicationEventPublisher applicationEventPublisher;

  /**
   * 밴더, 서브벤더, 섹션 정보를 입력 받기 위한 환경설정 화면을 표시함
   * 
   * @param model
   * @return
   */
  @GetMapping("/vender/setup")
  public String setupForm(Model model) {
    return "vender_setup";
  }

  /**
   * 환경설정 화면에서 벤더,서브벤더,섹션 정보를 입력 받아 섹션의 대기자 큐에서 대기자를 꺼내, 대기자 조작 화면을 표시함
   * 
   * @param form
   * @param model
   * @return
   */
  @GetMapping("/vender/display")
  public String displayForm(
      @RequestParam String venderId,
      @RequestParam String subVenderId,
      @RequestParam String sectionId, 
      Model model) {

    Objects.requireNonNull(venderId);
    Objects.requireNonNull(subVenderId);
    Objects.requireNonNull(sectionId);

    var vender = findVender(venderId);
    var subVender = findSubVender(subVenderId, vender);
    var section = findSection(sectionId, subVender);

    var polledSection = section.poll();
    log.info(polledSection.toString());

    model.addAttribute("myUser", polledSection);
    model.addAttribute("section", section);
    model.addAttribute("venderId", venderId);
    model.addAttribute("subVenderId", subVenderId);
    model.addAttribute("fromSectionId", sectionId);
    model.addAttribute("createdAt", 
    polledSection.isPresent() ? polledSection.get().getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) : "");
    
    return "vender_display";
  }

  /**
   * 벤더,서브벤더,섹션 정보를 입력 받아 대기자를 이동 또는 삭제함
   * 
   * @param form
   * @param fromSectionId
   * @param toSectionId   사용자가 선택한 섹션
   * @param userId
   * @return
   * @throws JsonProcessingException
   */
  @PostMapping("/vender/{fromSectionId}/{userId}")
  public String processUser(
      FormDto form,
      @PathVariable String fromSectionId,
      @PathVariable String userId
  ) throws JsonProcessingException {

    log.info(form.toString());

    var vender = findVender(form.getVenderId());
    var subVender = findSubVender(form.getSubVenderId(), vender);

    var fromSection = findSection(fromSectionId, subVender);
    var toSectionId = form.getSectionId();

    Section toSection = null;

    if (!toSectionId.equals("NONE")) {
      toSection = findSection(toSectionId, subVender);
    }

    // 지정된 색션의 대기자 큐에서 꺼냄
    var polledFromSection = fromSection.poll().orElseThrow();
    if (toSectionId.equals("NONE")) {
      // Do nothing !!!
    } else {
      // 새로운 색션의 대기자 큐로 이동함
      toSection.add(new MyUser(polledFromSection.getId(), toSection.getWaitingNumber(),
          polledFromSection.getCreatedAt()));
    }
    venderRepo.save(vender);

    if (toSection == null) {
      publishEvent(userId, "END_OPERATION");
    } else {
      var jsonString = mapper.writeValueAsString(new EmitterNotifyAllDto(form.getVenderId(),
          form.getSubVenderId(), fromSectionId, toSectionId));
      publishEvent(userId, jsonString);
    }

    return "redirect:/vender/display?" + "venderId=" + form.getVenderId() + "&" + "subVenderId="
        + form.getSubVenderId() + "&" + "sectionId=" + fromSectionId;
  }

  private Vender findVender(String venderId) {
    return venderRepo.findByName(venderId.toUpperCase());
  }

  private SubVender findSubVender(String subVenderId, Vender vender) {
    return vender.getSubVenders().stream()
        .filter(v -> v.getName().equals(subVenderId.toUpperCase())).findFirst().orElseThrow();
  }

  private Section findSection(String sectionId, SubVender subVender) {
    return subVender.getSections().stream().filter(s -> s.getName().equals(sectionId.toUpperCase()))
        .findFirst().orElseThrow();
  }

  private void publishEvent(String userId, String messages) {
    SectionChangedEvent event = new SectionChangedEvent(this, userId, messages);
    applicationEventPublisher.publishEvent(event);
  }

}
