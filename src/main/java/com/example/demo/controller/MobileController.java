package com.example.demo.controller;

import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;
import com.example.demo.entity.Section;
import com.example.demo.entity.SubVender;
import com.example.demo.entity.Vender;
import com.example.demo.repository.VenderRepository;
import com.example.demo.service.SseEmitterService;
import com.example.demo.vo.FormDto;
import com.example.demo.vo.MyUser;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import lombok.RequiredArgsConstructor;

/**
 * 스마트폰에서 동작하는 웹앱의 동작을 담당하는 콘트롤러
 */
@Controller
@CrossOrigin("*")
@RequiredArgsConstructor
public class MobileController {

  private final VenderRepository venderRepo;
  private final SseEmitterService emitterService;

  /**
   * 벤더,서브벤더,섹션 정보를 입력 받아 스마트폰에서 웹앱을 기동한다.
   * 
   * @param venderId
   * @param subVenderId
   * @param sectionId
   * @param model
   * @return
   */
  @GetMapping("/webapp/{venderId}/{subVenderId}/{sectionId}")
  public String runWebApp(
      @PathVariable String venderId,
      @PathVariable String subVenderId,
      @PathVariable String sectionId, Model model) {
    
    model.addAttribute("venderId", venderId);
    model.addAttribute("subVenderId", subVenderId);
    model.addAttribute("sectionId", sectionId);
    
    return "mobile";
  }

  /**
   * 스마트폰에서 벤더,서브벤더,섹션 정보를 입력 받아 해당 섹션의 큐에 사용자를 추가한 후 DB에 저장
   * 
   * @param form
   * @return
   */
  @PostMapping("/webapp")
  public ResponseEntity<Section> addUser(@RequestBody FormDto form) {
    
    var vender = findVender(form.getVenderId());
    var subVender = findSubVender(form.getSubVenderId(), vender);
    var section = findSection(form.getSectionId(), subVender);

    section.add(new MyUser(randomUUID().toString(), section.getWaitingNumber(), now()));
    venderRepo.save(vender);

    return ResponseEntity.ok(section);
  }
  
  @DeleteMapping("/webapp/{userId}")
  public ResponseEntity<Void> cancelWaiting(
      @RequestBody FormDto form, 
      @PathVariable String userId) {

    var vender = findVender(form.getVenderId());
    var subVender = findSubVender(form.getSubVenderId(), vender);
    var section = findSection(form.getSectionId(), subVender);
    var myUser = findMyUser(userId, section);
    section.getWaitingUsers().remove(myUser);
    venderRepo.save(vender);
    
    emitterService.remove(userId);

    return ResponseEntity.ok().build();
  }
  
  @GetMapping("/webapp/{venderId}/{subVenderId}/{sectionId}/section")
  public ResponseEntity<Section> getSection(
      @PathVariable String venderId,
      @PathVariable String subVenderId,
      @PathVariable String sectionId) {

    var vender = findVender(venderId);
    var subVender = findSubVender(subVenderId, vender);
    var section = findSection(sectionId, subVender);

    return ResponseEntity.ok(section);
  }

  private Vender findVender(String venderId) {
    return venderRepo.findByName(venderId);
  }

  private SubVender findSubVender(String subVenderId, Vender vender) {
    return vender.getSubVenders().stream().filter(v -> v.getName().equals(subVenderId)).findFirst()
        .orElseThrow();
  }
  
  private Section findSection(String sectionId, SubVender subVender) {
    return subVender.getSections().stream().filter(s -> s.getName().equals(sectionId)).findFirst()
        .orElseThrow();
  }

  private MyUser findMyUser(String userId, Section section) {
    return section.getWaitingUsers().stream().filter(u -> u.getId().equals(userId)).findAny()
    .orElseThrow();
  }
  
}