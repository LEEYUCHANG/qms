package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 접수 단말기
 */
@Controller
public class TerminalController {

  /**
   * QR코드를 만들기 위해 벤더,서브벤더,섹션 정보를 입력 받아
   * 접수 단말기 화면을 표시함
   * @param venderId
   * @param subVenderId
   * @param sectionId
   * @param model
   * @return
   */
  @GetMapping("/terminal/{venderId}/{subVenderId}/{sectionId}")
  public String terminal(
      @PathVariable String venderId,
      @PathVariable String subVenderId,
      @PathVariable String sectionId, 
      Model model) {

    model.addAttribute("venderId", venderId.toUpperCase());
    model.addAttribute("subVenderId", subVenderId.toUpperCase());
    model.addAttribute("sectionId", sectionId.toUpperCase());
    
    return "terminal";
  }
}
