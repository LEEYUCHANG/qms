package com.example.demo.controller;

import java.util.Objects;
import com.example.demo.service.SseEmitterService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

@Log
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/emitter")
public class SseEmitterController {

  private final SseEmitterService emiterService;

  @GetMapping
  public SseEmitter eventEmitter(@RequestParam String userId) {

    log.warning("emitter start!! with " + userId);
    Objects.requireNonNull(userId);

    SseEmitter emitter = new SseEmitter(1800000L); // important!! this is the timeout
    emiterService.add(userId, emitter);

    emitter.onCompletion(() -> emiterService.remove(emitter));
    emitter.onTimeout(() -> emiterService.remove(emitter));

    return emitter;
  }

  @GetMapping("/notify")
  public void notify(@RequestParam String userId) {
    log.warning("emitter notify!! with " + userId);
    Objects.requireNonNull(userId);
    emiterService.notify(userId, "notify called!!");
  }

  @GetMapping("/show")
  public void show() {
    log.warning("show called!!");
    emiterService.showMap();
  }

  @GetMapping("/clear")
  public void clear() {
    log.warning("clear called!!");
    emiterService.reset();
  }

}
