package com.example.demo.controller;

import static com.example.demo.util.Util.getQRCodeImage;
import static java.lang.String.join;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * QR코드 담당
 */
@Controller
public class QRcodeController {

  @Value("${webapp.endpoint}")
  private String WEBAPP_ENDPOINT;

  /**
   * 벤더,서브벤더,섹션 정보를 입력 받아 접속을 위한 QR코드를 만듬
   * 
   * @param venderId
   * @param subVenderId
   * @param sectionId
   * @param response
   * @throws IOException
   */
  @GetMapping("/qrcode/{venderId}/{subVenderId}/{sectionId}")
  public void qrcode(
      @PathVariable String venderId,
      @PathVariable String subVenderId,
      @PathVariable String sectionId, 
      HttpServletResponse response) throws IOException {

    response.setContentType("image/png");
    OutputStream outputStream = response.getOutputStream();
    outputStream.write(getQRCodeImage(join("/", WEBAPP_ENDPOINT, venderId, subVenderId, sectionId), 250, 250));
    outputStream.flush();
    outputStream.close();
  } 
}
