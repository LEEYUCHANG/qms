package com.example.demo.event;

import org.springframework.context.ApplicationEvent;
import lombok.Getter;

@Getter
public class SectionChangedEvent extends ApplicationEvent {
  
  private static final long serialVersionUID = 1L;
  private final String userId;
  private final String message;
  
  public SectionChangedEvent(Object source, String userId, String message) {
    super(source);
    this.userId = userId;
    this.message = message;
  }

}
