package com.example.demo.vo;

import java.time.LocalDateTime;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(of = "id")
@RequiredArgsConstructor
public class MyUser {
  private final String id;
  private final Integer myNumber;
  private final LocalDateTime createdAt;
}
