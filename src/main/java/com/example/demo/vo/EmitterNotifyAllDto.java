package com.example.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EmitterNotifyAllDto {
  private String venderId;
  private String subVenderId;
  private String fromSectionId;
  private String toSectionId;
}
