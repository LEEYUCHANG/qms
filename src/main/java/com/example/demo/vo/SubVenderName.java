package com.example.demo.vo;

public enum SubVenderName {
  NONE,
  LA,
  LASVEGAS,
  MICHIGAN,
}
