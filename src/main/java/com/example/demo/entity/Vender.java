package com.example.demo.entity;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Document
@Getter
@RequiredArgsConstructor
public class Vender {
  @Id
  private String id;
  private final String name;
  private final List<SubVender> subVenders;
}
