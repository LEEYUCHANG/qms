package com.example.demo.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.example.demo.vo.MyUser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public class Section {

  private final String name;
  private Integer waitingCounter = 0;
  private List<MyUser> waitingUsers = new ArrayList<>();

  public Optional<MyUser> poll() {
    return waitingUsers.size() > 0 ? Optional.of(waitingUsers.remove(0)) : Optional.empty();   
  }

  public void add(MyUser user) {
    waitingUsers.add(user);
    countUpWaitingNumber();
  }

  public Integer getWaitingNumber() {
    return waitingCounter + 1;
  }

  private void countUpWaitingNumber() {
    ++waitingCounter;
  }
  

}
