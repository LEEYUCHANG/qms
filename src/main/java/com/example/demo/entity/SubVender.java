package com.example.demo.entity;

import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SubVender {
  private final String name;
  private final List<Section> sections;
}
