package com.example.demo;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import com.example.demo.entity.Section;
import com.example.demo.entity.SubVender;
import com.example.demo.entity.Vender;
import com.example.demo.repository.VenderRepository;
import com.example.demo.util.Util;
import com.example.demo.vo.MyUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import lombok.extern.java.Log;

@Log
@SpringBootTest
class DemoApplicationTests {

	@Autowired
	VenderRepository venderRepo;

	@Test
	@Commit
	void setupDocument() {
		List<Section> sectionList = new ArrayList<>();
		sectionList.add(new Section("RECEPTION"));
		sectionList.add(new Section("INTERNAL"));
		sectionList.add(new Section("EXTERNAL"));
		List<SubVender> subVenderList = new ArrayList<>();
		subVenderList.add(new SubVender("LA", sectionList));
		Vender vender = new Vender("WOORI", subVenderList);
		venderRepo.save(vender);
	}

	@Test
	void addEntiry() {
		var vender = venderRepo.findByName("WOORI");
		var subVender = vender.getSubVenders().stream().filter(sub -> sub.getName().equals("LA"))
				.findFirst().orElseThrow();
		var section = subVender.getSections().stream().filter(sec -> sec.getName().equals("RECEPTION"))
				.findFirst().orElseThrow();
		section.add(new MyUser(UUID.randomUUID().toString(),section.getWaitingCounter()+1, LocalDateTime.now()));
		venderRepo.save(vender);
	}

	@Test
	void pollEntity() {
		var vender = venderRepo.findByName("WOORI");
		var subVender = vender.getSubVenders().stream().filter(sub -> sub.getName().equals("LA"))
				.findFirst().orElseThrow();
		var section = subVender.getSections().stream().filter(sec -> sec.getName().equals("RECEPTION"))
				.findFirst().orElseThrow();
		section.poll();
		venderRepo.save(vender);
	}

	@Test
	void md5() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		log.info(Util.hashByMd5("f3a59d96df8a379132483c03e340f74c") + "");
	}


}
